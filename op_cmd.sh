#! /bin/sh



if [ $1 = '-rsa' ]
then
   python rsa_crypt.py -e cmd.sh pub.pem cmd.rsa
   echo 'output: cmd.rsa'
elif [ $1 = '-eaes' ]
then
   if [ -e cmd.aes ]
   then
      mv cmd.aes "cmd.aes.`date +%Y%m%d%S`"
   fi
   python mycrypt.py -e -i cmd.sh -o cmd.aes
   echo 'output: cmd.aes'
elif [ $1 = '-daes' ]
then
   if [ -e cmd.sh ]
   then
      mv cmd.sh "cmd.sh.`date +%Y%m%d%S`"
   fi
   python mycrypt.py -d -i cmd.aes -o cmd.sh
   echo 'output: cmd.sh'
fi
